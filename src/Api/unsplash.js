import axios from 'axios';

export default axios.create({
    baseURL:'https://api.unsplash.com',
    headers:{
        Authorization: 'Client-ID 1G1GYwo-Fs4WxMT0Mgye7lQZhiYMFReIiRFOIQ9K9NU'
    }
});