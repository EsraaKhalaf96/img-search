import React from 'react'
import unsplash from '../Api/unsplash'
import ImageList from './ImageList';
import SearchBar from './SearchBar'
import ImageCard from './ImageCard'

class App  extends React.Component{
    state ={images:[]};
    onSearchSubmit =(term)=>{
        unsplash.get('/search/photos',{
            params:{query: term}
        }).then(res=>{
            console.log(res)
           this.setState({
            images: res.data.results
           })
        });
    }
    render(){
        return(
            <div className="ui container"   style={{marginTop: "15px"}}>
                <SearchBar onSubmit ={this.onSearchSubmit}/>
                <ImageList images={this.state.images}/>
                {/* <ImageCard /> */}
            </div>
    )
    }

}

export default App;