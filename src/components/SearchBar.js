import React from 'react'

class SearchBar extends React.Component{
    state ={term:''}
     onSubmitForm = (e)=>{
        e.preventDefault();
         //console.log(this.state.term);
        this.props.onSubmit(this.state.term)
    }
    onHandelchange=(e)=>{
     this.setState({
         term :e.target.value 
        })
    }
    render(){
        return(
            <div className="ui segment">
                <form onSubmit={this.onSubmitForm} className="ui form">
                    <div className="field">
                        <label>search  for any image </label>
                    <input type="text" value={this.state.term} onChange={this.onHandelchange}/>
                    </div>
                </form>
            </div>
        );
    }
}


export default SearchBar;